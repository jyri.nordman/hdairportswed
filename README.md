<p align="center">
<a href="http://www.rortos.it/"><img src="http://www.rortos.it/wp-content/uploads/2020/01/face.jpg"/></a>
</p>


# Rortos WED HD Airports

In this repository you can find the WED files utilized by [Real flight simulator](http://www.rortos.it/real-flight-simulator/) for building airports.


## RFS 
Live a unique experience flying in any part of the world and exploring sceneries and airports in high resolution with satellite maps, 3D buildings, runways, procedures and air traffic. Jump on board of real time flights, chat with other pilots and join them in multiplayer. Manage flight plans and interact with ATC controllers. Get access to thousands of community created liveries, customize all airplanes, their gauges, failures and weather conditions. Become a real pilot!

![rfs](http://www.rortos.it/wp-content/uploads/2020/01/01-1.png)

## Download RFS from stores
<p align="center">
<a href="https://apps.apple.com/us/app/rfs-real-flight-simulator/id1444761746"><img src="http://www.rortos.it/wp-content/uploads/2020/01/app.png" hspace="10"/></a> <a href="https://play.google.com/store/apps/details?id=it.rortos.realflightsimulator"><img src="http://www.rortos.it/wp-content/uploads/2020/01/google.png"/></a></p>

## Licensing and Copyright
All the contents in this repository are under the [GPL-2](https://www.gnu.org/licenses/gpl-2.0.txt) license


